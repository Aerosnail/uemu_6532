#include <command.h>

#include <commands.h>
#include <utils.h>
#include <log/log.h>

#include "riot.h"

typedef struct {
	const char *cmd;
	const char *help;
	int (*fcn)(int, const char*[], void*);
} command_t;

static int map_ram(int argc, const char *argv[], void *ctx);
static int map_io(int argc, const char *argv[], void *ctx);

static const command_t cmds[] = {
	{
		.cmd = "map ram",
		.help = "Map internal RAM",
		.fcn = map_ram
	},
	{
		.cmd = "map ports",
		.help = "Map IO ports",
		.fcn = map_io
	}
};

int
register_commands(const char *name, struct riot *ctx)
{
	int ret = 0;
	size_t i;

	for (i=0; i<LEN(cmds); i++) {
		ret |= cmd_register_module(name,
				cmds[i].cmd, cmds[i].help, cmds[i].fcn, ctx);
	}

	return ret;
}

int
unregister_commands(const char *name)
{
	return cmd_unregister_module(name, NULL);
}

static int
map_ram(int argc, const char *argv[], void *ctx)
{
	struct riot *self = (struct riot*)ctx;
	unsigned long long start_addr;

	if (argc < 2) {
		log_warn("Usage: map ram <start addr>");
		return 1;
	}

	start_addr = strtoull(argv[1], NULL, 16);

	return riot_map_ram(self, start_addr);
}

static int
map_io(int argc, const char *argv[], void *ctx)
{
	int ret = 0;
	struct riot *self = (struct riot*)ctx;
	unsigned long long start_addr;

	if (argc < 2) {
		log_warn("Usage: map ports <start addr>");
		return 1;
	}

	start_addr = strtoull(argv[1], NULL, 16);

	ret |= riot_map_ports(self, start_addr);
	ret |= riot_map_timer(self, start_addr + 0x4);

	return ret;
}

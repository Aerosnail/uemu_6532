#pragma once

#include "riot.h"

int register_commands(const char *name, struct riot *ctx);
int unregister_commands(const char *name);

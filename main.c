#include <assert.h>
#include <string.h>
#include <stdlib.h>

#include <module.h>

#include "commands.h"
#include "riot.h"

static const char *_compatible[] = {
	NULL
};

__global
ModuleInfo __info = {
	.name = "6532",
	.author = "Aerosnail",
	.major = VERSION_MAJOR,
	.minor = VERSION_MINOR,
	.patch = VERSION_PATCH,
	.compatible = _compatible
};

__global int
__init(void)
{
	return 0;
}

__global void*
__create_instance(DTBModuleSpecs *specs)
{
	struct riot *self = riot_init(specs->mem_mgr, specs->name);

	self->name = strdup(specs->name);
	register_commands(specs->name, self);

	return self;
}

__global int
__delete_instance(void *ctx)
{
	int ret = 0;
	struct riot *self = (struct riot*)ctx;

	ret |= unregister_commands(self->name);
	free(self->name);
	ret |= riot_deinit(self);

	return ret;
}

__global int
__deinit(void)
{
	return 0;
}

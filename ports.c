#include <stdlib.h>
#include <string.h>

#include <log/log.h>
#include <utils.h>

#include "ports.h"

#define IS_OUTPUT(ddr, idx) (ddr & (1 << idx))

enum reg {
	REG_DDRA,
	REG_DDRB,
	REG_OUTA,
	REG_OUTB,
};

static enum reg address_to_register(uint64_t addr);

int
riot_ports_init(struct port_regs *self, const char *module_name)
{
	size_t i;
	char pin_name[4];

	self->ddra = 0;
	self->ddrb = 0;

	for (i=0; i<LEN(self->pa); i++) {
		sprintf(pin_name, "pa%d", (int)i);
		self->pa[i] = gpio_create_instance(module_name,
		                                   pin_name,
		                                   GPIO_TYPE_INPUT,
		                                   1,
		                                   NULL,
		                                   NULL);
	}

	for (i=0; i<LEN(self->pb); i++) {
		sprintf(pin_name, "pb%d", (int)i);
		self->pb[i] = gpio_create_instance(module_name,
		                                   pin_name,
		                                   GPIO_TYPE_INPUT,
		                                   1,
		                                   NULL,
		                                   NULL);
	}

	return 0;
}

int
riot_ports_deinit(struct port_regs *self)
{
	size_t i;

	for (i=0; i<LEN(self->pa); i++) {
		gpio_delete_instance(self->pa[i]);
	}
	for (i=0; i<LEN(self->pb); i++) {
		gpio_delete_instance(self->pb[i]);
	}

	return 0;
}

int
riot_ports_read(void *ctx, uint64_t addr, void *vdata, size_t len)
{
	struct port_regs *self = (struct port_regs*)ctx;
	size_t i, j;
	uint8_t *data = (uint8_t*)vdata;
	int_fast8_t value;

	addr &= 0x03;

	if (addr + len > PORTS_ADDR_SPACE) return 1;

	for (i=0; i<len; i++) {
		switch (address_to_register(addr + i)) {
		case REG_DDRA:
			data[i] = self->ddra;
			break;

		case REG_DDRB:
			data[i] = self->ddrb;
			break;

		case REG_OUTA:
			value = 0;
			for (j=0; j<LEN(self->pa); j++) {
				value |= gpio_read(self->pa[j]) << j;
			}
			data[i] = value;
			break;

		case REG_OUTB:
			value = 0;
			for (j=0; j<LEN(self->pb); j++) {
				value |= gpio_read(self->pb[j]) << j;
			}
			data[i] = value;
			break;
		}
	}

	return 0;
}

int
riot_ports_write(void *ctx, uint64_t addr, const void *vdata, size_t len)
{
	struct port_regs *self = (struct port_regs*)ctx;
	uint8_t *data = (uint8_t*)vdata;
	size_t i, j;

	enum gpio_type direction;
	int_fast8_t value;

	if (addr + len > PORTS_ADDR_SPACE) return 1;

	for (i=0; i<len; i++) {
		switch (address_to_register(addr + i)) {
		case REG_DDRA:
			self->ddra = data[i];

			for (j=0; j<LEN(self->pa); j++) {
				direction = self->ddra & (1 << j) ? GPIO_TYPE_OUTPUT : GPIO_TYPE_INPUT;
				gpio_set_type(self->pa[j], direction);
			}
			break;

		case REG_DDRB:
			self->ddrb = data[i];

			for (j=0; j<LEN(self->pb); j++) {
				direction = self->ddrb & (1 << j) ? GPIO_TYPE_OUTPUT : GPIO_TYPE_INPUT;
				gpio_set_type(self->pb[j], direction);
			}
			break;

		case REG_OUTA:
			/* Update pins */
			for (j=0; j<LEN(self->pa); j++) {
				value = data[i] & (1 << j) ? 1 : 0;
				gpio_write(self->pa[j], value);
			}

			break;

		case REG_OUTB:
			/* Update pins */
			for (j=0; j<LEN(self->pb); j++) {
				value = data[i] & (1 << j) ? 1 : 0;
				gpio_write(self->pb[j], value);
			}

			break;
		}
	}

	return 0;
}

/* Static functions {{{ */
static enum reg
address_to_register(uint64_t addr)
{
	const enum reg registers[] = { REG_OUTA, REG_DDRA, REG_OUTB, REG_DDRB };
	return registers[addr & 0x3];
}
/* }}} */

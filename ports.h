#pragma once

#include <stdint.h>
#include <stdlib.h>

#include <gpio.h>

#define PORTS_ADDR_SPACE 4

struct port_regs {
	uint8_t ddra;   /* Data direction register A */
	uint8_t ddrb;   /* Data direction register B */

	/* Actual pins */
	GPIO* pa[8];
	GPIO* pb[8];
};

int riot_ports_init(struct port_regs *self, const char *name);
int riot_ports_deinit(struct port_regs *self);
int riot_ports_read(void *ctx, uint64_t addr, void *data, size_t len);
int riot_ports_write(void *ctx, uint64_t addr, const void *data, size_t len);

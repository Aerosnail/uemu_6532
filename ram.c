#include <string.h>

#include "riot.h"
#include "ram.h"

int
riot_ram_init(struct ram *self)
{
	memset(self->data, 0, sizeof(self->data));
	return 0;
}

int
riot_ram_deinit(struct ram *self)
{
	(void)self;
	return 0;
}

int
riot_ram_read(void *ctx, uint64_t addr, void *data, size_t len)
{
	struct ram *self = (struct ram*)ctx;

	if (addr + len > sizeof(self->data)) return 1;

	memcpy(data, self->data + addr, len);
	return 0;
}

int
riot_ram_write(void *ctx, uint64_t addr, const void *data, size_t len)
{
	struct ram *self = (struct ram*)ctx;

	if (addr + len > sizeof(self->data)) return 1;

	memcpy(self->data + addr, data, len);
	return 0;
}

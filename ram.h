#pragma once

#include <stdint.h>
#include <stdlib.h>

struct ram {
	uint8_t data[128];
};

int riot_ram_init(struct ram *self);
int riot_ram_deinit(struct ram *self);
int riot_ram_read(void *ctx, uint64_t addr, void *data, size_t len);
int riot_ram_write(void *ctx, uint64_t addr, const void *data, size_t len);


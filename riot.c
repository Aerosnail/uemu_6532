#include <string.h>

#include "riot.h"
#include "ram.h"

struct riot*
riot_init(MemManager *mgr, const char *name)
{
	struct riot *self = calloc(1, sizeof(*self));
	if (!self) return NULL;

	self->mgr = mgr;
	riot_ram_init(&self->ram);
	riot_ports_init(&self->ports, name);
	riot_timer_init(&self->timer);

	return self;
}

int
riot_deinit(struct riot *self)
{
	int ret = 0;

	riot_ram_deinit(&self->ram);
	riot_ports_deinit(&self->ports);
	riot_timer_deinit(&self->timer);

	ret |= mem_unregister(self->mgr, self->ram_entry);

	ret |= mem_unregister(self->mgr, self->port_entries[0]);
	ret |= mem_unregister(self->mgr, self->port_entries[1]);
	ret |= mem_unregister(self->mgr, self->port_entries[2]);
	ret |= mem_unregister(self->mgr, self->port_entries[3]);

	ret |= mem_unregister(self->mgr, self->timer_entries[0]);
	ret |= mem_unregister(self->mgr, self->timer_entries[1]);

	free(self);

	return ret;
}

int
riot_map_ram(struct riot *self, uint64_t start_addr)
{
	struct mem_handler handler;

	handler.read = riot_ram_read;
	handler.write = riot_ram_write;
	handler.execute = riot_ram_read;
	handler.ctx = &self->ram;

	self->ram_entry = mem_register(self->mgr, &handler, start_addr, sizeof(self->ram));
	return self->ram_entry == NULL;
}

int
riot_map_ports(struct riot *self, uint64_t start_addr)
{
	struct mem_handler handler;

	handler.read = riot_ports_read;
	handler.write = riot_ports_write;
	handler.execute = riot_ports_read;
	handler.ctx = &self->ports;

	self->port_entries[0] = mem_register(self->mgr, &handler,
			start_addr, PORTS_ADDR_SPACE);
	self->port_entries[1] = mem_register(self->mgr, &handler,
			start_addr + 0x08, PORTS_ADDR_SPACE);
	self->port_entries[2] = mem_register(self->mgr, &handler,
			start_addr + 0x10, PORTS_ADDR_SPACE);
	self->port_entries[3] = mem_register(self->mgr, &handler,
			start_addr + 0x18, PORTS_ADDR_SPACE);

	return !self->port_entries[0] || !self->port_entries[1] ||
	       !self->port_entries[2] || !self->port_entries[3];
}


int
riot_map_timer(struct riot *self, uint64_t start_addr)
{
	struct mem_handler handler;

	handler.read = riot_timer_read;
	handler.write = riot_timer_write;
	handler.execute = riot_timer_read;
	handler.ctx = &self->timer;

	self->timer_entries[0] = mem_register(self->mgr, &handler, start_addr, TIMER_ADDR_SPACE);
	self->timer_entries[1] = mem_register(self->mgr, &handler, start_addr + 0x08, TIMER_ADDR_SPACE);

	return !self->timer_entries[0] || !self->timer_entries[1];
}


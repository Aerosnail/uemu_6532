#pragma once

#include <stdint.h>

#include <memory.h>

#include "ram.h"
#include "ports.h"
#include "timer.h"

struct riot {
	char *name;

	MemManager *mgr;
	MemEntry *ram_entry, *port_entries[4], *timer_entries[2];

	struct ram ram;
	struct port_regs ports;
	struct timer timer;
};

struct riot* riot_init(MemManager *mgr, const char *name);
int riot_deinit(struct riot *self);

int riot_map_ram(struct riot *self, uint64_t start_addr);
int riot_map_ports(struct riot *self, uint64_t start_addr);
int riot_map_timer(struct riot *self, uint64_t start_addr);

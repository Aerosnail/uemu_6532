#pragma once

#include <stdint.h>
#include <stdlib.h>

#define TIMER_ADDR_SPACE 0x04

struct timer {
	uint8_t _placeholder;
};

int riot_timer_init(struct timer *self);
int riot_timer_deinit(struct timer *self);
int riot_timer_read(void *ctx, uint64_t addr, void *data, size_t len);
int riot_timer_write(void *ctx, uint64_t addr, const void *data, size_t len);
